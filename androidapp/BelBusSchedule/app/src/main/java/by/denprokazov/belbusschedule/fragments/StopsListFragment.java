package by.denprokazov.belbusschedule.fragments;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.sql.SQLException;
import java.util.List;

import by.denprokazov.belbusschedule.DAOs.DAOManager;
import by.denprokazov.belbusschedule.R;
import by.denprokazov.belbusschedule.StopInfoActivity;
import by.denprokazov.belbusschedule.StopPickerActivity;
import by.denprokazov.belbusschedule.viewAdapters.StopsInfoAdapter;
import by.denprokazov.belbusschedule.viewAdapters.StopsListAdapter;
import by.denprokazov.belbusschedule.viewModels.StopsListModel;

public class StopsListFragment extends Fragment {
    private final int CITY_ID = 1;
    private List<StopsListModel> stopsListModels;
    private Context context;

    public StopsListFragment() {
        //this.CITY_ID = CITY_ID;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("Остановки");
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        StopsListAdapter stopsListAdapter = null;
        ListView listView = (ListView) view.findViewById(R.id.main_list_view);
        DAOManager daoManager = new DAOManager();
        try {
            if(calledFromFavourites()) {
                stopsListModels = daoManager.getFavouriteStopList();
            } else {
                stopsListModels = daoManager.getStopsListModelByCity(CITY_ID);
            }
            stopsListAdapter = new StopsListAdapter(this.getActivity(), stopsListModels, false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        listView.setAdapter(stopsListAdapter);
        listView.setOnItemClickListener(getStopClickListener());
    }

    private AdapterView.OnItemClickListener getStopClickListener() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                StopInfoFragment stopInfoFragment = new StopInfoFragment(stopsListModels.get(position).getStopName());
//                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.container, stopInfoFragment);
//                fragmentTransaction.addToBackStack("stops_list");
//                fragmentTransaction.commit();

                // TODO: 4/4/16 maybe refuse dialog activity and use full activity
                Intent intent = new Intent(getActivity().getApplicationContext(), StopInfoActivity.class);
                intent.putExtra("stop_name", stopsListModels.get(position).getStopName());
                startActivity(intent);
            }
        };
    }

    private boolean calledFromFavourites() {
        Bundle bundle = getArguments();
        boolean isFavourite = false;
        if (bundle != null) {
            isFavourite = bundle.getBoolean("fromFavourite");
        }
        return isFavourite;
    }
}
