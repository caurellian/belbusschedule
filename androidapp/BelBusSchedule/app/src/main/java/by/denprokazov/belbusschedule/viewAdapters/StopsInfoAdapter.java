package by.denprokazov.belbusschedule.viewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import by.denprokazov.belbusschedule.R;
import by.denprokazov.belbusschedule.viewModels.StopTimetableModel;

public class StopsInfoAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final List<StopTimetableModel> stopTimetableModels;

    public StopsInfoAdapter(Context context, List<StopTimetableModel> stopTimetableModels) {
        super(context, R.layout.stop_info_item);
        this.context = context;
        this.stopTimetableModels = stopTimetableModels;
    }

    static class ViewHolder {
        TextView routeNameTV;
        TextView routeDirectionTV;
        TextView regularRouteTimetableTV;
        TextView holidayRouteTimetableTV;
    }

    @Override
    public int getCount() {
        return stopTimetableModels.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.stop_info_item, parent, false);
            viewHolder.routeNameTV = (TextView) view.findViewById(R.id.route_name_textview);
            viewHolder.routeDirectionTV = (TextView) view.findViewById(R.id.route_direction_textview);
            viewHolder.regularRouteTimetableTV = (TextView) view.findViewById(R.id.regular_timetable_textview);
            viewHolder.holidayRouteTimetableTV = (TextView) view.findViewById(R.id.holiday_timetable_textview);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        fillViewHolderItems(position, viewHolder);
        return view;
    }

    private void fillViewHolderItems(int position, ViewHolder viewHolder) {
        StopTimetableModel stopTimetableModel = stopTimetableModels.get(position);
        viewHolder.routeNameTV.setText(stopTimetableModel.getRouteName());
        viewHolder.routeDirectionTV.setText(stopTimetableModel.getDirectionName());
        viewHolder.regularRouteTimetableTV.setText(stopTimetableModel.getRegularTimetable());
        viewHolder.holidayRouteTimetableTV.setText(stopTimetableModel.getHolidayTimetable());
    }
}
