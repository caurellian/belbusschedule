package by.denprokazov.belbusschedule;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.sql.SQLException;
import java.util.List;

import by.denprokazov.belbusschedule.DAOs.DAOManager;
import by.denprokazov.belbusschedule.fragments.StopTimetableFragment;
import by.denprokazov.belbusschedule.viewAdapters.StopsListAdapter;
import by.denprokazov.belbusschedule.viewModels.StopsListModel;

public class StopPickerActivity extends AppCompatActivity {
    private int route_id_forward;
    private int route_id_backward;
    private List<StopsListModel> forwardStopsListModels;
    private List<StopsListModel> backwardStopsListModels;
    private ListView forwardStopsLV;
    private ListView backwardStopsLV;
    private String route_name;
    private String route_from;
    private String route_to;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stops_picker);
        getRouteIdsFromIntentExtra();
        setTitle(route_name + " " + route_from + " <> " + route_to);
        initWaitUI();
        initUI();
    }

    private void initWaitUI() {
        forwardStopsLV = (ListView) findViewById(R.id.forward_stops);
        backwardStopsLV = (ListView) findViewById(R.id.backwards_stops);
        forwardStopsLV.setVisibility(View.GONE);
        backwardStopsLV.setVisibility(View.GONE);
    }

    private void initUI() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                getStopListModels();
                forwardStopsLV.post(new Runnable() {
                    @Override
                    public void run() {
                        removeProgressBar();
                        setUpForwardListView(forwardStopsLV);
                    }
                });
                backwardStopsLV.post(new Runnable() {
                    @Override
                    public void run() {
                        removeProgressBar();
                        setUpBackwardListView(backwardStopsLV);
                    }
                });
            }
        }).start();
    }

    private void removeProgressBar() {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.stop_picker_progress_bar);
        progressBar.setVisibility(View.GONE);
    }

    private void setUpForwardListView(ListView forwardStopsLV) {
        StopsListAdapter forwardStopsListAdapter = new StopsListAdapter(this, forwardStopsListModels, true);
        forwardStopsLV.setVisibility(View.VISIBLE);
        forwardStopsLV.setAdapter(forwardStopsListAdapter);
        View separateLineView = (View) findViewById(R.id.separate_line);
        separateLineView.setVisibility(View.VISIBLE);
        forwardStopsLV.setOnItemClickListener(getForwardStopItemClickListener());

    }

    private void setUpBackwardListView(ListView backwardStopsLV) {
        StopsListAdapter backwardStopsListAdapter= new StopsListAdapter(this, backwardStopsListModels, true);
        backwardStopsLV.setVisibility(View.VISIBLE);
        backwardStopsLV.setAdapter(backwardStopsListAdapter);
        backwardStopsLV.setOnItemClickListener(getForwardStopItemClickListener());
    }

    private void getStopListModels() {
        DAOManager daoManager = new DAOManager();
        try {
            forwardStopsListModels = daoManager.getStopsListModelByRoute(route_id_forward);
            backwardStopsListModels = daoManager.getStopsListModelByRoute(route_id_backward);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private AdapterView.OnItemClickListener getBackwardStopItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setContentView(R.layout.stop_timetable);
                setTitle(backwardStopsListModels.get(position).getStopName());
                StopTimetableFragment stopTimetableFragment = new StopTimetableFragment(backwardStopsListModels.get(position).getStopId()
                        ,route_id_backward);
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.timetable_container, stopTimetableFragment);
                fragmentTransaction.commit();
            }
        };
    }

    private AdapterView.OnItemClickListener getForwardStopItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setContentView(R.layout.stop_timetable);
                setTitle(forwardStopsListModels.get(position).getStopName());
                StopTimetableFragment stopTimetableFragment = new StopTimetableFragment(forwardStopsListModels.get(position).getStopId()
                ,route_id_forward);
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.timetable_container, stopTimetableFragment);
                fragmentTransaction.commit();
            }
        };
    }

    private void getRouteIdsFromIntentExtra() {
        Intent intent = getIntent();
        route_id_forward = intent.getIntExtra("route_id_forward", 0);
        route_id_backward = intent.getIntExtra("route_id_backward", 0);
        route_name = intent.getStringExtra("route_name");
        route_from = intent.getStringExtra("route_from");
        route_to = intent.getStringExtra("route_to");
    }
}
