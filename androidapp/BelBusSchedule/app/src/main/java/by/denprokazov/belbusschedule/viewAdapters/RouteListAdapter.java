package by.denprokazov.belbusschedule.viewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import by.denprokazov.belbusschedule.R;
import by.denprokazov.belbusschedule.viewModels.RouteLabelModel;
import by.denprokazov.belbusschedule.viewModels.RouteListModel;

public class RouteListAdapter extends ArrayAdapter<String> {
    private final Context context;
    List<RouteListModel> routeListModels;

    public RouteListAdapter(Context context, List<RouteListModel> routeListModels) {
        super(context, R.layout.route_list_item);
        this.context = context;
        this.routeListModels = routeListModels;
    }

    public RouteLabelModel getRouteLabelModel(int position) {
        RouteLabelModel routeLabelModel = new RouteLabelModel();
        routeLabelModel.setRouteName(routeListModels.get(position).getRouteName());
        routeLabelModel.setStartStop(routeListModels.get(position).getfStopFrom());
        routeLabelModel.setEndStop(routeListModels.get(position).getfStopTo());
        return routeLabelModel;
    }

    @Override
    public int getCount() {
        return routeListModels.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // TODO: 04.03.2016 IMPLEMENT VIEW HOLDER YOU DUMB IDIOT
        View view = inflater.inflate(R.layout.route_list_item, parent, false);

        TextView routeTV = (TextView) view.findViewById(R.id.route_name);
        TextView fDirFromTV = (TextView) view.findViewById(R.id.first_direction_from);
        TextView fDirToTV = (TextView) view.findViewById(R.id.first_direction_to);
        TextView sDirFromTV = (TextView) view.findViewById(R.id.second_direction_from);
        TextView sDirToTV = (TextView) view.findViewById(R.id.second_direction_to);

        RouteListModel routeListModel = routeListModels.get(position);
        setTextViewValues(routeTV, fDirFromTV, fDirToTV, sDirFromTV,sDirToTV, routeListModel);
        return view;
    }

    private void setTextViewValues(TextView routeTV, TextView fDirFromTV, TextView fDirToTV, TextView sDirFromTV, TextView sDirToTV, RouteListModel routeListModel) {
        routeTV.setText(routeListModel.getRouteName());
        fDirFromTV.setText(routeListModel.getfStopFrom());
        fDirToTV.setText(routeListModel.getfStopTo());
        sDirFromTV.setText(routeListModel.getsStopFrom());
        sDirToTV.setText(routeListModel.getsStopTo());
    }

    public int[] getRoutesIdsByModel(int position) {
        int[] routeIdsByModel = new int[3];
        // TODO: 09.03.2016 make this return enum class maybe?
        routeIdsByModel[0] = routeListModels.get(position).getRouteId();
        routeIdsByModel[1] = routeListModels.get(position).getBackwardRouteId();
        return routeIdsByModel;
    }
}
