package by.denprokazov.belbusschedule.DAOs;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.denprokazov.belbusschedule.structures.FavouriteRoute;

public class FavouriteRoutesDAO extends BaseDaoImpl<FavouriteRoute, Integer> {
    public FavouriteRoutesDAO(ConnectionSource connectionSource, Class<FavouriteRoute> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Integer> getFavouriteRoutes() throws SQLException {
        QueryBuilder<FavouriteRoute, Integer> favouriteRoutesIntegerQueryBuilder = queryBuilder();
        PreparedQuery<FavouriteRoute> preparedQuery = favouriteRoutesIntegerQueryBuilder.prepare();
        List<FavouriteRoute> favouriteRoutes = query(preparedQuery);
        List<Integer> favouriteRoutesIds = new ArrayList<>();
        for (FavouriteRoute favouriteRoute : favouriteRoutes) {
            favouriteRoutesIds.add(favouriteRoute.route_id);
        }
        return favouriteRoutesIds;
    }
}
