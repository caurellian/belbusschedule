package by.denprokazov.belbusschedule.viewModels;

public class RouteListModel {
    public RouteListModel(String routeName, String fDirectionFrom, String fDirectionTo, String sDirectionFrom, String sDirectionTo, int routeId, int backwardRouteId) {
        this.routeName = routeName;
        this.fStopFrom = fDirectionFrom;
        this.fStopTo = fDirectionTo;
        this.sStopFrom = sDirectionFrom;
        this.sStopTo = sDirectionTo;
        this.routeId = routeId;
        this.backwardRouteId = backwardRouteId;
    }

    private String routeName;
    private String fStopFrom;
    private String fStopTo;
    private String sStopTo;
    private String sStopFrom;
    private int routeId;
    private int backwardRouteId;

    public int getRouteId() {
        return routeId;
    }

    public int getBackwardRouteId() {
        return backwardRouteId;
    }


    public String getRouteName() {
        return routeName;
    }

    public String getfStopFrom() {
        return fStopFrom;
    }

    public String getfStopTo() {
        return fStopTo;
    }

    public String getsStopFrom() {
        return sStopFrom;
    }

    public String getsStopTo() {
        return sStopTo;
    }
}
