package by.denprokazov.belbusschedule;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.denprokazov.belbusschedule.DAOs.DAOManager;
import by.denprokazov.belbusschedule.viewModels.CitiesModel;

public class SettingsActivity extends AppCompatActivity {

    private List<CitiesModel> citiesModels;
    public static final String SHARED_SETTINGS = "settings_shared_prefs";
    public static final String SHARED_SETTINGS_CHOOSED_CITY_ID = "city_id";
    private SharedPreferences mSharedPrefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSharedPrefs = getSharedPreferences(SHARED_SETTINGS, Context.MODE_PRIVATE);
        initUI();
    }

    private void initUI() {
        setContentView(R.layout.settings);
        setTitle(getString(R.string.settings_activity_title));

        List<String> cityNames = new ArrayList<>();
        getCitiesModel();
        for (int i = 0; i < citiesModels.size(); i++) {
            cityNames.add(citiesModels.get(i).getCityName());
        }

        Spinner citySpinner = (Spinner) findViewById(R.id.city_choose_spinner);
        ArrayAdapter<String> cityAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, cityNames);
        citySpinner.setAdapter(cityAdapter);
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences.Editor editor = mSharedPrefs.edit();
                editor.putString(SHARED_SETTINGS_CHOOSED_CITY_ID, String.valueOf(citiesModels.get(position).getCityId()));
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void getCitiesModel() {
        DAOManager daoManager = new DAOManager();
        try {
            citiesModels = daoManager.getCitiesModel();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
