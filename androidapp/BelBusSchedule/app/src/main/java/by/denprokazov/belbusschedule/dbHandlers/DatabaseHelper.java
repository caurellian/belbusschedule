package by.denprokazov.belbusschedule.dbHandlers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;

import org.json.JSONException;
import java.sql.SQLException;

import by.denprokazov.belbusschedule.DAOs.FavouriteRoutesDAO;
import by.denprokazov.belbusschedule.DAOs.FavouritesStopsDAO;
import by.denprokazov.belbusschedule.DAOs.TypesDAO;
import by.denprokazov.belbusschedule.structures.City;
import by.denprokazov.belbusschedule.structures.Country;
import by.denprokazov.belbusschedule.DAOs.CitiesDAO;
import by.denprokazov.belbusschedule.DAOs.CountriesDAO;
import by.denprokazov.belbusschedule.DAOs.RoutesDAO;
import by.denprokazov.belbusschedule.DAOs.RoutesNStopDAO;
import by.denprokazov.belbusschedule.DAOs.StopsDAO;
import by.denprokazov.belbusschedule.structures.FavouriteRoute;
import by.denprokazov.belbusschedule.structures.FavouriteStop;
import by.denprokazov.belbusschedule.structures.Route;
import by.denprokazov.belbusschedule.structures.RoutenStop;
import by.denprokazov.belbusschedule.structures.Stop;
import by.denprokazov.belbusschedule.structures.Type;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "scheduleDB.db";
    private final SQLiteDatabase db;
    private StopsDAO stopsDAO = null;
    private RoutesDAO routesDAO = null;
    private RoutesNStopDAO routesNStopDAO = null;
    private CitiesDAO citiesDAO = null;
    private CountriesDAO countriesDAO = null;
    private TypesDAO typesDAO = null;
    private FavouritesStopsDAO favouritesStopsDAO = null;
    private FavouriteRoutesDAO favouriteRoutesDAO = null;

    public DatabaseHelper(Context context) throws JSONException, SQLException {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
    }


    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
    }

    @Override
    public void close() {
        super.close();
    }

    private void  fillDatabase(String jsonString) throws JSONException, SQLException {
    }

    public StopsDAO getStopsDAO() throws SQLException {
        if (stopsDAO == null) {
            stopsDAO = new StopsDAO(getConnectionSource(), Stop.class);
        }
        return stopsDAO;
    }

    public RoutesDAO getRoutesDAO() throws SQLException {
        if(routesDAO == null) {
            routesDAO = new RoutesDAO(getConnectionSource(), Route.class);
        }
        return routesDAO;
    }

    public RoutesNStopDAO getRoutesNStopDAO() throws SQLException {
        if (routesNStopDAO == null) {
            routesNStopDAO = new RoutesNStopDAO(getConnectionSource(), RoutenStop.class);
        }
        return routesNStopDAO;
    }

    public CitiesDAO getCitiesDAO() throws SQLException {
        if (citiesDAO == null) {
            citiesDAO = new CitiesDAO(getConnectionSource(), City.class);
        }
        return citiesDAO;
    }

    public CountriesDAO getCountriesDAO() throws SQLException {
        if (countriesDAO == null) {
            countriesDAO = new CountriesDAO(getConnectionSource(), Country.class);
        }
        return countriesDAO;
    }

    public TypesDAO getTypesDAO() throws SQLException {
        if(typesDAO == null) {
            typesDAO = new TypesDAO(getConnectionSource(), Type.class);
        }
        return typesDAO;
    }

    public FavouritesStopsDAO getFavouritesStopsDAO() throws SQLException {
        if(favouritesStopsDAO == null) {
            favouritesStopsDAO = new FavouritesStopsDAO(getConnectionSource(), FavouriteStop.class);
        }
        return favouritesStopsDAO;
    }

    public FavouriteRoutesDAO getFavouriteRoutesDAO() throws SQLException {
        if(favouriteRoutesDAO == null) {
            favouriteRoutesDAO = new FavouriteRoutesDAO(getConnectionSource(), FavouriteRoute.class);
        }
        return favouriteRoutesDAO;
    }
}
