package by.denprokazov.belbusschedule.DAOs;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.denprokazov.belbusschedule.structures.RoutenStop;

public class RoutesNStopDAO extends BaseDaoImpl<RoutenStop, Integer> {
    public RoutesNStopDAO(ConnectionSource connectionSource, Class<RoutenStop> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Integer> getStopIdsByRoute(int route_id) throws SQLException {
        QueryBuilder<RoutenStop, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("route_id", route_id);
        queryBuilder.groupBy("stop_id");
        List<Integer> stopIds = new ArrayList<>();
        PreparedQuery<RoutenStop>  preparedQuery = queryBuilder.prepare();
        List<RoutenStop> routenStops = query(preparedQuery);
        for (int i = 0; i < routenStops.size(); i++) {
            stopIds.add(routenStops.get(i).stop_id);
        }
        return stopIds;
    }

    public List<RoutenStop> getRoutenStopByStopAndRoute(int stop_id, int route_id) throws SQLException {
        List<String> timeTable = new ArrayList<>();
        List<RoutenStop> routenStops;
        QueryBuilder<RoutenStop, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("stop_id", stop_id).and().eq("route_id", route_id);
        PreparedQuery<RoutenStop> preparedQuery = queryBuilder.prepare();
        routenStops = query(preparedQuery);

        return routenStops;
    }

    public List<RoutenStop> getRoutenStopByStopId(int stop_id) throws SQLException {
        List<String> timetable = new ArrayList<>();
        List<RoutenStop> routenStopList;
        QueryBuilder<RoutenStop, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("stop_id", stop_id);
        PreparedQuery<RoutenStop> preparedQuery = queryBuilder.prepare();
        routenStopList = query(preparedQuery);
        return routenStopList;
    }

    public List<Integer> getRouteIdsByStopIds(List<Integer> stopIds) throws SQLException {
        List<Integer> routeIds = new ArrayList<>();
        List<RoutenStop> routenStops;
        PreparedQuery<RoutenStop> preparedQuery;
        QueryBuilder<RoutenStop, Integer> queryBuilder = queryBuilder();
        for (int i = 0; i < stopIds.size(); i++) {
            queryBuilder.where().eq("stop_id", Integer.toString(stopIds.get(i)));
            queryBuilder.groupBy("route_id");
            preparedQuery = queryBuilder.prepare();
            routenStops = query(preparedQuery);
            for (int n = 0; n < routenStops.size(); n++) {
                routeIds.add(routenStops.get(n).route_id);
            }
        }
        return routeIds;
    }
}
