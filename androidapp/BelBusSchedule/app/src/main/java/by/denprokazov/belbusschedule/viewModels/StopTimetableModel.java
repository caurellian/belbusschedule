package by.denprokazov.belbusschedule.viewModels;

public class StopTimetableModel {
    public StopTimetableModel(String routeName, String directionName, String regularTimetable, String holidayTimetable) {
        this.routeName = routeName;
        this.directionName = directionName;
        this.regularTimetable = regularTimetable;
        this.holidayTimetable = holidayTimetable;
    }

    private String routeName;
    private String directionName;
    private String regularTimetable;
    private String holidayTimetable;

    public String getHolidayTimetable() {
        return holidayTimetable;
    }

    public String getRouteName() {
        return routeName;
    }

    public String getDirectionName() {
        return directionName;
    }

    public String getRegularTimetable() {
        return regularTimetable;
    }
}
