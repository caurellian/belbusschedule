package by.denprokazov.belbusschedule.jsonHandlers;

import android.support.annotation.NonNull;

public interface JsonParselableItem {
    @NonNull
    String getJsonArrayName();
    Class getClassInstance();
}
