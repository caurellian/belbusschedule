package by.denprokazov.belbusschedule.structures;

import android.support.annotation.NonNull;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import by.denprokazov.belbusschedule.jsonHandlers.JsonParselableItem;


@DatabaseTable(tableName = "Types")
public class Type implements JsonParselableItem {
    private static final String JSON_ARRAY_NAME = "Types";

    @DatabaseField(columnName = "Id", generatedId = true)
    public int type_id;
    @DatabaseField(columnName = "name")
    public String type;

    @NonNull
    @Override
    public String getJsonArrayName() {
        return JSON_ARRAY_NAME;
    }

    @Override
    public Class getClassInstance() {
        return Type.class;
    }
}
