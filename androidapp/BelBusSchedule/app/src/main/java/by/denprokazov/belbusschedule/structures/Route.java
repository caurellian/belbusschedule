package by.denprokazov.belbusschedule.structures;

import android.support.annotation.NonNull;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import by.denprokazov.belbusschedule.jsonHandlers.JsonParselableItem;


@DatabaseTable(tableName = "Routes")
public class Route implements JsonParselableItem{
    private static final String JSON_ARRAY_NAME = "Routes";
    @DatabaseField(columnName = "Id", generatedId = true)
    public int route_id;
    @DatabaseField(columnName = "name")
    public String routeName;
    @DatabaseField(columnName = "type_id")
    public int type_id;
    @DatabaseField(columnName = "city_id")
    public int city_id;
    @DatabaseField(columnName = "begin_stop_id")
    public int begin_stop_id;
    @DatabaseField(columnName = "end_stop_id")
    public int end_stop_id;

    public Route() {
    }


    @NonNull
    @Override
    public String getJsonArrayName() {
        return JSON_ARRAY_NAME;
    }

    @Override
    public Class getClassInstance() {
        return Route.class;
    }
}
