package by.denprokazov.belbusschedule.structures;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "FavouriteStops")
public class FavouriteStop {
    @DatabaseField(columnName = "stop_id")
    public int stop_id;
}
