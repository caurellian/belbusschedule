package by.denprokazov.belbusschedule.structures;

import android.support.annotation.NonNull;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import by.denprokazov.belbusschedule.jsonHandlers.JsonParselableItem;


@DatabaseTable(tableName = "Cities")
public class City implements JsonParselableItem {
    private static final String JSON_ARRAY_NAME = "Cities";
    @DatabaseField(columnName = "Id", generatedId = true)
    public int city_id;

    @DatabaseField(columnName = "name")
    public String cityName;

    @DatabaseField(columnName = "country_id")
    public int country_id;

    public City() {

    }

    @NonNull
    @Override
    public String getJsonArrayName() {
        return JSON_ARRAY_NAME;
    }

    @Override
    public Class getClassInstance() {
        return City.class;
    }
}
