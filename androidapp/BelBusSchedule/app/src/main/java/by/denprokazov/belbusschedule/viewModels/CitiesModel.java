package by.denprokazov.belbusschedule.viewModels;

public class CitiesModel {

    private String cityName;
    private int cityId;

    public CitiesModel(String cityName, int cityId) {
        this.cityName = cityName;
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public int getCityId() {
        return cityId;
    }
}
