package by.denprokazov.belbusschedule.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.sql.SQLException;

import by.denprokazov.belbusschedule.DAOs.DAOManager;
import by.denprokazov.belbusschedule.R;
import by.denprokazov.belbusschedule.StopPickerActivity;
import by.denprokazov.belbusschedule.viewAdapters.RouteListAdapter;

public class RouteListFragment extends Fragment {
    private final int CITY_ID = 1;
    private ListView listView;
    private RouteListAdapter routeListAdapter;

    public RouteListFragment() {
        //this.CITY_ID = CITY_ID;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle("Маршруты");
        return inflater.inflate(R.layout.main_fragment, container, false);
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = (ListView) view.findViewById(R.id.main_list_view);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if(getActivity()!= null) {
                        DAOManager daoManager = new DAOManager();
                        if (calledFromFavourites()) {
                                routeListAdapter = new RouteListAdapter(getContext(), daoManager
                                        .getFavouriteRouteList());
                        } else {
                            routeListAdapter = new RouteListAdapter(getContext(), daoManager
                                    .getRouteListModelByCity(CITY_ID));
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                listView.post(new Runnable() {
                    @Override
                    public void run() {
                        listView.setAdapter(routeListAdapter);
                        final RouteListAdapter finalRouteListAdapter = routeListAdapter;
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent dialogIntent = new Intent(getActivity(), StopPickerActivity.class);
                                if (finalRouteListAdapter != null) {
                                    putRouteIdsToIntentExtra(position, dialogIntent);
                                }
                                listView.setEnabled(false);
                                startActivity(dialogIntent);
                            }
                            private void putRouteIdsToIntentExtra(int position, Intent dialogIntent) {
                                dialogIntent.putExtra("route_name", routeListAdapter.getRouteLabelModel(position).getRouteName());
                                dialogIntent.putExtra("route_to", routeListAdapter.getRouteLabelModel(position).getStartStop());
                                dialogIntent.putExtra("route_from", routeListAdapter.getRouteLabelModel(position).getEndStop());
                                dialogIntent.putExtra("route_id_forward", finalRouteListAdapter.getRoutesIdsByModel(position)[0]);
                                dialogIntent.putExtra("route_id_backward", finalRouteListAdapter.getRoutesIdsByModel(position)[1]);
                            }
                        });
                    }
                });
            }
        }).start();
    }

    @Override
    public void onResume() {
        super.onResume();
        listView.setEnabled(true);
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    private boolean calledFromFavourites() {
        Bundle bundle = getArguments();
        boolean isFavourite = false;
        if (bundle != null) {
            isFavourite = bundle.getBoolean("fromFavourite");
        }
        return isFavourite;
    }
}
