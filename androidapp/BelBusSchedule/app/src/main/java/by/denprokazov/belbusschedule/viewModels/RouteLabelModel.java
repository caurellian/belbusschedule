package by.denprokazov.belbusschedule.viewModels;

public class RouteLabelModel {
    private String routeName;
    private String startStop;
    private String endStop;

    public String getRouteName() {
        return routeName;
    }

    public String getStartStop() {
        return startStop;
    }

    public String getEndStop() {
        return endStop;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public void setStartStop(String startStop) {
        this.startStop = startStop;
    }

    public void setEndStop(String endStop) {
        this.endStop = endStop;
    }
}
