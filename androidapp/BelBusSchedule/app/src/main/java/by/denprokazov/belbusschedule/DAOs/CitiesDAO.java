package by.denprokazov.belbusschedule.DAOs;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import by.denprokazov.belbusschedule.structures.City;

public class CitiesDAO extends BaseDaoImpl<City, Integer> {

    public CitiesDAO(ConnectionSource connectionSource, Class<City> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<City> getCities() throws SQLException {
        QueryBuilder<City, Integer> queryBuilder = queryBuilder();
        PreparedQuery<City> preparedQuery = queryBuilder.prepare();
        List<City> cities = query(preparedQuery);
        return cities;
    }
}
