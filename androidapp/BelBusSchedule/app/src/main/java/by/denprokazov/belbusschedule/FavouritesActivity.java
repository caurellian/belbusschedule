package by.denprokazov.belbusschedule;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import by.denprokazov.belbusschedule.fragments.RouteListFragment;
import by.denprokazov.belbusschedule.fragments.StopsListFragment;

public class FavouritesActivity  extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favourites_layout);
        initUI();
    }

    private void initUI() {
        initToolBar();
        Bundle favBundle = new Bundle();
        favBundle.putBoolean("fromFavourite", true);
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(getSupportFragmentManager(),
                FragmentPagerItems.with(this)
                        .add(R.string.route_tab_name, RouteListFragment.class, favBundle)
                        .add(R.string.stops_tab_name, StopsListFragment.class, favBundle).create());


        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);
    }

    private void initToolBar() {
        Toolbar mainToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        mainToolbar.setTitle("Отобраное");
        setSupportActionBar(mainToolbar);
    }
}
