package by.denprokazov.belbusschedule.dbHandlers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class DBInitializer extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "scheduleDB.db";
    private static final int DATABASE_VERSION = 1;

    public DBInitializer(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO: 19.02.2016 maybe put db on sdcard?
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    @Override
    public synchronized void close() {
        super.close();
    }
}
