package by.denprokazov.belbusschedule.structures;

import android.support.annotation.NonNull;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import by.denprokazov.belbusschedule.jsonHandlers.JsonParselableItem;


@DatabaseTable(tableName = "Stops")
public class Stop implements JsonParselableItem{
    private static final String JSON_ARRAY_NAME = "Stops";
    @DatabaseField(columnName = "Id")
    public int stop_id;

    @DatabaseField(columnName = "name")
    public String stopName;

    @DatabaseField(columnName = "city_id")
    public int city_id;

    public Stop() {
    }

    public String getStopName() {
        return stopName;
    }

    @NonNull
    @Override
    public String getJsonArrayName() {
        return JSON_ARRAY_NAME;
    }

    @Override
    public Class getClassInstance() {
        return Stop.class;
    }
}
