package by.denprokazov.belbusschedule;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.sql.SQLException;
import java.util.List;

import by.denprokazov.belbusschedule.DAOs.DAOManager;
import by.denprokazov.belbusschedule.viewAdapters.StopsInfoAdapter;
import by.denprokazov.belbusschedule.viewModels.StopTimetableModel;

public class StopInfoActivity extends AppCompatActivity {

    private String stopName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_fragment);

        getStopNameFromExtra();
        initUI();
    }

    private void initUI() {
        StopsInfoAdapter stopsInfoAdapter = null;
        ListView stopInfoLV = (ListView) findViewById(R.id.main_list_view);
        DAOManager daoManager = new DAOManager();
        try {
            List<StopTimetableModel> stopTimetables = daoManager.getStopTimetables(stopName);
            stopsInfoAdapter = new StopsInfoAdapter(this, stopTimetables);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        stopInfoLV.setAdapter(stopsInfoAdapter);
    }

    private void getStopNameFromExtra() {
        Intent intent = getIntent();
        stopName = intent.getStringExtra("stop_name");
    }
}
