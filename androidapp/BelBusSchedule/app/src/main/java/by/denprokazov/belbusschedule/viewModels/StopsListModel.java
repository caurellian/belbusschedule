package by.denprokazov.belbusschedule.viewModels;

public class StopsListModel {
    public StopsListModel(String stopName, int stopId) {
        this.stopName = stopName;
        this.stopId = stopId;
    }

    private String stopName;
    private int stopId;

    public String getStopName() {
        return stopName;
    }

    public int getStopId() {
        return stopId;
    }
}
