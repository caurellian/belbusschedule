package by.denprokazov.belbusschedule.DAOs;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.denprokazov.belbusschedule.structures.FavouriteStop;

public class FavouritesStopsDAO extends BaseDaoImpl<FavouriteStop, Integer>{
    public FavouritesStopsDAO(ConnectionSource connectionSource, Class<FavouriteStop> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Integer> getFavouriteStops() throws SQLException {
        QueryBuilder<FavouriteStop, Integer> favouriteStopsIntegerQueryBuilder= queryBuilder();
        PreparedQuery<FavouriteStop> preparedQuery = favouriteStopsIntegerQueryBuilder.prepare();
        List<FavouriteStop> favouriteStopses =  query(preparedQuery);
        List<Integer> favouriteStopsIds = new ArrayList<>();
        for (FavouriteStop favouriteStop : favouriteStopses) {
            favouriteStopsIds.add(favouriteStop.stop_id);
        }
        return favouriteStopsIds;
    }
}
