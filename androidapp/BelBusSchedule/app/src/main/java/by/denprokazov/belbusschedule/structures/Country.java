package by.denprokazov.belbusschedule.structures;

import android.support.annotation.NonNull;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import by.denprokazov.belbusschedule.jsonHandlers.JsonParselableItem;


@DatabaseTable(tableName = "Countries")
public class Country implements JsonParselableItem{
    private static final String JSON_ARRAY_NAME = "Countries";
    @DatabaseField(columnName = "Id", generatedId = true)
    public int country_id;
    @DatabaseField(columnName = "name")
    public String countryName;

    public Country() {
    }

    @NonNull
    @Override
    public String getJsonArrayName() {
        return JSON_ARRAY_NAME;
    }

    @Override
    public Class getClassInstance() {
        return Country.class;
    }
}
