package by.denprokazov.belbusschedule.dbHandlers;

import org.json.JSONException;

import java.sql.SQLException;
import java.util.ArrayList;

import by.denprokazov.belbusschedule.HelperFactory;
import by.denprokazov.belbusschedule.jsonHandlers.JSONHandler;
import by.denprokazov.belbusschedule.structures.City;
import by.denprokazov.belbusschedule.structures.Country;

public class DBFiller {
    // TODO: 01.03.2016 move to new structures for gson

    private final String jsonString;

    public DBFiller(String jsonString) throws JSONException, SQLException {
        this.jsonString  = jsonString;
    }

    public void fillEntireDB() throws JSONException, SQLException {
        fillCountries(jsonString);
        fillCities(jsonString);
    }

    private void fillCities(String jsonString) throws JSONException, SQLException {
        City city = new City();
        JSONHandler<City> citiesJSONHandler = new JSONHandler<>(city);
        ArrayList<City> cities = new ArrayList<>();
        cities = citiesJSONHandler.parseJsonString(this.jsonString);
        for(int i = 0; i < cities.size(); i++) {
            HelperFactory.getDatabaseHelper().getCitiesDAO().create(cities.get(i));
        }
    }

    private void fillCountries(String jsonString) throws JSONException, SQLException {
        Country country = new Country();
        JSONHandler<Country> countriesJSONHandler = new JSONHandler<>(country);
        ArrayList<Country> countries = new ArrayList<>();
        countries = countriesJSONHandler.parseJsonString(jsonString);
        for (int i = 0; i < countries.size(); i++) {
            HelperFactory.getDatabaseHelper().getCountriesDAO().create(countries.get(i));
        }
    }
}
