package by.denprokazov.belbusschedule.DAOs;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import by.denprokazov.belbusschedule.structures.Type;

public class TypesDAO extends BaseDaoImpl<Type, Integer> {
    public TypesDAO(ConnectionSource connectionSource, Class<Type> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
