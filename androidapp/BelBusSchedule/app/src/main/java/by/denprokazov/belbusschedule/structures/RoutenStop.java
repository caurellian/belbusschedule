package by.denprokazov.belbusschedule.structures;

import android.support.annotation.NonNull;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import by.denprokazov.belbusschedule.jsonHandlers.JsonParselableItem;


@DatabaseTable(tableName = "RoutesnStop")
public class RoutenStop implements JsonParselableItem {
    private static final String JSON_ARRAY_NAME = "RoutesnStop";
    @DatabaseField(columnName = "Id")
    public int Id;
    @DatabaseField(columnName = "route_id")
    public int route_id;
    @DatabaseField(columnName = "stop_id")
    public int stop_id;
    @DatabaseField(columnName = "time")
    public String time;
    @DatabaseField(columnName = "is_holiday")
    public int isHoliday;

    public RoutenStop() {
    }

    @NonNull
    @Override
    public String getJsonArrayName() {
        return JSON_ARRAY_NAME;
    }

    @Override
    public Class getClassInstance() {
        return RoutenStop.class;
    }
}
