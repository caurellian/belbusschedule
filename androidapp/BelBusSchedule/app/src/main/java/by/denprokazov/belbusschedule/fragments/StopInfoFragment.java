package by.denprokazov.belbusschedule.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.sql.SQLException;
import java.util.List;

import by.denprokazov.belbusschedule.DAOs.DAOManager;
import by.denprokazov.belbusschedule.R;
import by.denprokazov.belbusschedule.viewAdapters.StopsInfoAdapter;
import by.denprokazov.belbusschedule.viewModels.StopTimetableModel;

public class StopInfoFragment extends Fragment {

    private final String stopName;

    public StopInfoFragment(String stopName) {
        this.stopName = stopName;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(stopName);
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        StopsInfoAdapter stopsInfoAdapter = null;
        ListView stopInfoLV = (ListView) view.findViewById(R.id.main_list_view);
        DAOManager daoManager = new DAOManager();
        try {
            List<StopTimetableModel> stopTimetables = daoManager.getStopTimetables(stopName);
            stopsInfoAdapter = new StopsInfoAdapter(this.getActivity(), stopTimetables);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        stopInfoLV.setAdapter(stopsInfoAdapter);
    }
}
