package by.denprokazov.belbusschedule;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import by.denprokazov.belbusschedule.dbHandlers.DatabaseHelper;

public class HelperFactory {
    private static DatabaseHelper databaseHelper;

    public static DatabaseHelper getDatabaseHelper() {
        return databaseHelper;
    }

    public static void setDatabaseHelper(Context applicationContext) {
        databaseHelper = OpenHelperManager.getHelper(applicationContext, DatabaseHelper.class);
        //databaseHelper.setJsonString(jsonString);
        databaseHelper.getWritableDatabase();
    }

    public static void releaseHelper() {
        OpenHelperManager.releaseHelper();
        databaseHelper = null;
    }
}
