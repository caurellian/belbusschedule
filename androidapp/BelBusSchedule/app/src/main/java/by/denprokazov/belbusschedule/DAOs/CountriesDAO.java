package by.denprokazov.belbusschedule.DAOs;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import by.denprokazov.belbusschedule.structures.Country;

public class CountriesDAO extends BaseDaoImpl<Country, Integer> {
    public CountriesDAO(ConnectionSource connectionSource, Class<Country> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
