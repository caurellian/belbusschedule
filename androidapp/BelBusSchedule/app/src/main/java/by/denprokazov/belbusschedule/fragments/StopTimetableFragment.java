package by.denprokazov.belbusschedule.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.sql.SQLException;

import by.denprokazov.belbusschedule.DAOs.DAOManager;
import by.denprokazov.belbusschedule.R;
import by.denprokazov.belbusschedule.viewModels.StopTimetableModel;

public class StopTimetableFragment extends Fragment {

    private final int STOP_ID;
    private final int ROUTE_ID;

    public StopTimetableFragment(int STOP_ID, int ROUTE_ID) {
        this.STOP_ID = STOP_ID;
        this.ROUTE_ID = ROUTE_ID;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.stop_timetable_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        StopTimetableModel stopTimetableModel;
        DAOManager daoManager = new DAOManager();
        TextView regularTimetableTV = (TextView) view.findViewById(R.id.regular_timetable);
        TextView holidayTimetableTV = (TextView) view.findViewById(R.id.holiday_timetable);
        try {
            stopTimetableModel = daoManager.getStopsTimetable(STOP_ID, ROUTE_ID);
            regularTimetableTV.setText(stopTimetableModel.getRegularTimetable());
            holidayTimetableTV.setText(stopTimetableModel.getHolidayTimetable());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
