package by.denprokazov.belbusschedule.DAOs;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import by.denprokazov.belbusschedule.structures.Route;

public class RoutesDAO extends BaseDaoImpl<Route, Integer> {
    public RoutesDAO(ConnectionSource connectionSource, Class<Route> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Route> getRoutesByCity(int city_id) throws SQLException {
        QueryBuilder<Route, Integer> routesIntegerQueryBuilder = queryBuilder();
        routesIntegerQueryBuilder.where().eq("city_id", city_id);
        PreparedQuery<Route> preparedQuery = routesIntegerQueryBuilder.prepare();
        return query(preparedQuery);
    }

    public String getRouteNameByRouteId(int route_id) throws SQLException {
        QueryBuilder<Route, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("Id", route_id);
        PreparedQuery<Route> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery).get(0).routeName;
    }

    public int getEndStopByRouteId(int route_id) throws SQLException {
        QueryBuilder<Route, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("Id", route_id);
        PreparedQuery<Route> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery).get(0).end_stop_id;
    }

    public List<Route> getFavouriteRoutes(List<Integer> favouriteRoutes) throws SQLException {
        QueryBuilder<Route, Integer> routesIntegerQueryBuilder = queryBuilder();
        routesIntegerQueryBuilder.where().in("Id", favouriteRoutes);
        PreparedQuery<Route> preparedQuery = routesIntegerQueryBuilder.prepare();
        return query(preparedQuery);
    }
}
