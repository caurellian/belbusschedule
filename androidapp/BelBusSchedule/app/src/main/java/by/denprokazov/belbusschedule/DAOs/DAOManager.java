package by.denprokazov.belbusschedule.DAOs;

import android.support.annotation.NonNull;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.denprokazov.belbusschedule.HelperFactory;
import by.denprokazov.belbusschedule.structures.City;
import by.denprokazov.belbusschedule.structures.Route;
import by.denprokazov.belbusschedule.structures.RoutenStop;
import by.denprokazov.belbusschedule.structures.Stop;
import by.denprokazov.belbusschedule.viewModels.CitiesModel;
import by.denprokazov.belbusschedule.viewModels.RouteListModel;
import by.denprokazov.belbusschedule.viewModels.StopTimetableModel;
import by.denprokazov.belbusschedule.viewModels.StopsListModel;

public class DAOManager {
    public List<RouteListModel> getRouteListModelByCity(int city_id) throws SQLException {
        return getRouteListViewModels(city_id);
    }

    public List<RouteListModel> getFavouriteRouteList() throws SQLException {
        // TODO: 4/5/16 new adapter for routes(maybe new view)
        List<Route> routeList;
        List<Integer> favouriteRoutes;
        String routeName;
        String fStopFrom;
        String fStopTo;
        String sStopFrom;
        String sStopTo;
        int routeId;
        int backwardRouteId;

        favouriteRoutes = HelperFactory.getDatabaseHelper().getFavouriteRoutesDAO().getFavouriteRoutes();
        routeList = HelperFactory.getDatabaseHelper().getRoutesDAO().getFavouriteRoutes(favouriteRoutes);
        RouteListModel routeListModel;
        List<RouteListModel> routeListModels = new ArrayList<>();

        for (int i = 0; i < routeList.size(); i++ ) {
            routeName = routeList.get(i).routeName;
            routeId = routeList.get(i).route_id;
            fStopFrom = HelperFactory.getDatabaseHelper().getStopsDAO().getStopById(routeList.get(i).begin_stop_id).get(0).stopName;
            fStopTo = HelperFactory.getDatabaseHelper().getStopsDAO().getStopById(routeList.get(i).end_stop_id).get(0).stopName;
            for(int n = i+1; n < routeList.size(); n++) {
                if(routeList.get(i).routeName.equals(routeList.get(n).routeName)) {
                    sStopFrom = HelperFactory.getDatabaseHelper().getStopsDAO().getStopById(routeList.get(n).begin_stop_id).get(0).stopName;
                    sStopTo = HelperFactory.getDatabaseHelper().getStopsDAO().getStopById(routeList.get(n).end_stop_id).get(0).stopName;
                    backwardRouteId = routeList.get(n).route_id;
                    routeListModel = new RouteListModel(routeName, fStopFrom, fStopTo, sStopFrom, sStopTo, routeId, backwardRouteId);
                    routeListModels.add(routeListModel);
                }
            }
        }
        return routeListModels;
    }

    public List<StopsListModel> getStopsListModelByRoute(int route_id) throws SQLException {
        List<StopsListModel> stopsListModels = new ArrayList<>();
        List<Stop> stopList;
        List<Integer> stopsIds;
        String stopName;
        int stop_id;

        stopsIds = HelperFactory.getDatabaseHelper().getRoutesNStopDAO().getStopIdsByRoute(route_id);
        stopList = HelperFactory.getDatabaseHelper().getStopsDAO().getStopsListByIds(stopsIds);

        StopsListModel stopsListModel;
        for (int i = 0; i < stopsIds.size(); i++) {
            stopName = stopList.get(i).stopName;
            stop_id = stopList.get(i).stop_id;
            stopsListModel = new StopsListModel(stopName, stop_id);
            stopsListModels.add(stopsListModel);
        }
        return stopsListModels;
    }

    public List<StopsListModel> getStopsListModelByCity(int city_id) throws SQLException {
        List<StopsListModel> stopsListModels = new ArrayList<>();
        List<Stop> stopList;
        String stopName;
        int stop_id;

        stopList = HelperFactory.getDatabaseHelper().getStopsDAO().getStopsByCity(city_id);

        StopsListModel stopsListModel;
        for (int i = 0; i < stopList.size(); i++) {
            stopName = stopList.get(i).stopName;
            stop_id = stopList.get(i).stop_id;
            stopsListModel = new StopsListModel(stopName, stop_id);
            stopsListModels.add(stopsListModel);
        }
        return stopsListModels;
    }

    @NonNull
    private List<RouteListModel> getRouteListViewModels(int city_id) throws SQLException {
        List<Route> routeList;
        String routeName;
        String fStopFrom;
        String fStopTo;
        String sStopFrom;
        String sStopTo;
        int routeId;
        int backwardRouteId;

        routeList = HelperFactory.getDatabaseHelper().getRoutesDAO().getRoutesByCity(city_id);
        RouteListModel routeListModel;
        List<RouteListModel> routeListModels = new ArrayList<>();

        for (int i = 0; i < routeList.size(); i++ ) {
            routeName = routeList.get(i).routeName;
            routeId = routeList.get(i).route_id;
            fStopFrom = HelperFactory.getDatabaseHelper().getStopsDAO().getStopById(routeList.get(i).begin_stop_id).get(0).stopName;
            fStopTo = HelperFactory.getDatabaseHelper().getStopsDAO().getStopById(routeList.get(i).end_stop_id).get(0).stopName;
            for(int n = i+1; n < routeList.size(); n++) {
                if(routeList.get(i).routeName.equals(routeList.get(n).routeName)) {
                    sStopFrom = HelperFactory.getDatabaseHelper().getStopsDAO().getStopById(routeList.get(n).begin_stop_id).get(0).stopName;
                    sStopTo = HelperFactory.getDatabaseHelper().getStopsDAO().getStopById(routeList.get(n).end_stop_id).get(0).stopName;
                    backwardRouteId = routeList.get(n).route_id;
                    routeListModel = new RouteListModel(routeName, fStopFrom, fStopTo, sStopFrom, sStopTo, routeId, backwardRouteId);
                    routeListModels.add(routeListModel);
                }
            }
        }
        return routeListModels;
    }

    public StopTimetableModel getStopsTimetable(int stop_id, int route_id) throws SQLException {
        StopTimetableModel stopTimetableModel;
        String routeName;
        String directionName;
        String regularTimeTableString = "";
        String holidayTimeTableString  = "";
        List<RoutenStop> routenStops = HelperFactory.getDatabaseHelper().getRoutesNStopDAO().getRoutenStopByStopAndRoute(stop_id, route_id);

        for(int i = 0; i < routenStops.size(); i++) {
            if(routenStops.get(i).isHoliday == 0) {
                regularTimeTableString  = regularTimeTableString.concat(" " + routenStops.get(i).time);
            } else {
                holidayTimeTableString = holidayTimeTableString.concat(" " + routenStops.get(i).time);
            }
        }

        routeName = HelperFactory.getDatabaseHelper().getRoutesDAO().getRouteNameByRouteId(route_id);
        int endStopId = HelperFactory.getDatabaseHelper().getRoutesDAO().getEndStopByRouteId(route_id);
        directionName = HelperFactory.getDatabaseHelper().getStopsDAO().getStopNameById(endStopId);
        stopTimetableModel = new StopTimetableModel(routeName, directionName, regularTimeTableString, holidayTimeTableString);
        return stopTimetableModel;
    }

    public List<CitiesModel> getCitiesModel() throws SQLException {
        CitiesModel citiesModel;
        List<CitiesModel> citiesModels = new ArrayList<>();

        List<City> cities = HelperFactory.getDatabaseHelper().getCitiesDAO().getCities();
        for (int i = 0; i < cities.size(); i++) {
            citiesModel = new CitiesModel(cities.get(i).cityName, cities.get(i).city_id);
            citiesModels.add(citiesModel);
        }
        return citiesModels;
    }

    public List<StopTimetableModel> getStopTimetables(String stopName) throws SQLException {
        List<Integer> stopIds;
        List<Integer> routeIds;
        List<StopTimetableModel> stopTimetableModels = new ArrayList<>();
        stopIds = HelperFactory.getDatabaseHelper().getStopsDAO().getStopsIdsByStopName(stopName);
        routeIds = HelperFactory.getDatabaseHelper().getRoutesNStopDAO().getRouteIdsByStopIds(stopIds);
        StopTimetableModel stopTimetableModel;
        for (int i = 0; i < stopIds.size(); i++) {
            for (int n = 0; n < routeIds.size(); n++) {
                stopTimetableModel = getStopsTimetable(stopIds.get(i), routeIds.get(n));
                if(!stopTimetableModel.getRegularTimetable().equals("")
                        || !stopTimetableModel.getHolidayTimetable().equals("")) {
                    stopTimetableModels.add(stopTimetableModel);
                }
            }
        }
        return stopTimetableModels;
    }

    public List<StopsListModel> getFavouriteStopList() throws SQLException {
        List<StopsListModel> stopsListModels = new ArrayList<>();
        List<Stop> stopList;
        List<Integer> favouriteStopsIds;
        String stopName;
        int stop_id;

        favouriteStopsIds = HelperFactory.getDatabaseHelper()
                .getFavouritesStopsDAO().getFavouriteStops();
        stopList = HelperFactory.getDatabaseHelper().getStopsDAO().getFavouriteStops(favouriteStopsIds);

        StopsListModel stopsListModel;
        for (int i = 0; i < stopList.size(); i++) {
            stopName = stopList.get(i).stopName;
            stop_id = stopList.get(i).stop_id;
            stopsListModel = new StopsListModel(stopName, stop_id);
            stopsListModels.add(stopsListModel);
        }
        return stopsListModels;

    }
}
