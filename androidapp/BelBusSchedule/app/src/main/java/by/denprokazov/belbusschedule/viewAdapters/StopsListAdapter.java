package by.denprokazov.belbusschedule.viewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import by.denprokazov.belbusschedule.R;
import by.denprokazov.belbusschedule.viewModels.StopsListModel;

public class StopsListAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final boolean arrowNeeded;
    private List<StopsListModel> stopsListModels;
    private View view;

    static class ViewHolder {
        TextView stopNameTV;
        ImageView arrowImage;
    }

    public StopsListAdapter(Context context, List<StopsListModel> stopsListModels, boolean arrowNeeded) {
        super(context, R.layout.stops_picker_item);
        this.context = context;
        this.stopsListModels = stopsListModels;
        this.arrowNeeded = arrowNeeded;
    }

    @Override
    public int getCount() {
        return stopsListModels.size();
    }


    @Override
    public String getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public int getStopId(int position) {
        return stopsListModels.get(position).getStopId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view = convertView;
        if(view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.stops_picker_item, null, true);
            viewHolder = new ViewHolder();
            viewHolder.stopNameTV = (TextView) view.findViewById(R.id.stop_name_text_view);
            viewHolder.arrowImage = (ImageView) view.findViewById(R.id.direction_arrow);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        if (!arrowNeeded) {
            centerStopNameTV(viewHolder);
        }
        StopsListModel stopsListModel = stopsListModels.get(position);
        viewHolder.stopNameTV.setText(stopsListModel.getStopName());
        return view;
    }

    private void centerStopNameTV(ViewHolder viewHolder) {
        LinearLayout.LayoutParams imageLayoutParams = (LinearLayout.LayoutParams) viewHolder.arrowImage.getLayoutParams();
        LinearLayout.LayoutParams textLayoutParams = (LinearLayout.LayoutParams) viewHolder.stopNameTV.getLayoutParams();
        imageLayoutParams.weight = 0;
        imageLayoutParams.rightMargin = 0;
        textLayoutParams.weight = 1;
        textLayoutParams.rightMargin = 0;
        textLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        viewHolder.arrowImage.setLayoutParams(imageLayoutParams);
        viewHolder.stopNameTV.setLayoutParams(textLayoutParams);
    }
}
