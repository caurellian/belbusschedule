package by.denprokazov.belbusschedule.DAOs;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.denprokazov.belbusschedule.structures.Stop;

public class StopsDAO extends BaseDaoImpl<Stop, Integer> {
    public StopsDAO(ConnectionSource connectionSource, Class<Stop> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Stop> getStopsByCity(int cityID) throws SQLException {
        QueryBuilder<Stop, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("city_id", cityID);
        queryBuilder.groupBy("name");
        PreparedQuery<Stop> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }


    public List<Stop> getStopById(int stop_id) throws SQLException {
        QueryBuilder<Stop, Integer> stopsIntegerQueryBuilder = queryBuilder();
        stopsIntegerQueryBuilder.where().eq("Id", stop_id);
        PreparedQuery<Stop> preparedQuery = stopsIntegerQueryBuilder.prepare();
        return query(preparedQuery);
    }

    public List<Stop> getStopsListByIds(List<Integer> stopIds) throws SQLException {
        QueryBuilder<Stop, Integer> queryBuilder = queryBuilder();
        PreparedQuery<Stop> preparedQuery;
        List<Stop> stopList = new ArrayList<>();
        for (int i = 0; i <stopIds.size(); i++ ) {
            queryBuilder.where().eq("Id", stopIds.get(i));
            preparedQuery = queryBuilder.prepare();
            stopList.add(query(preparedQuery).get(0));
        }
        return stopList;
    }

    public List<Integer> getStopsIdsByStopName(String stopName) throws SQLException {
        QueryBuilder<Stop, Integer> queryBuilder = queryBuilder();
        PreparedQuery<Stop> preparedQuery;
        List<Stop> stopList;
        List<Integer> stopIds = new ArrayList<>();
        queryBuilder.where().eq("name", stopName);
        preparedQuery = queryBuilder.prepare();
        stopList = query(preparedQuery);
        for(int i = 0; i < stopList.size(); i++) {
            stopIds.add(stopList.get(i).stop_id);
        }
        return stopIds;
    }


    public String getStopNameById(int endStopId) throws SQLException {
        QueryBuilder<Stop, Integer> stopsIntegerQueryBuilder = queryBuilder();
        stopsIntegerQueryBuilder.where().eq("Id", endStopId);
        PreparedQuery<Stop> preparedQuery = stopsIntegerQueryBuilder.prepare();
        return query(preparedQuery).get(0).stopName;
    }

    public List<Stop> getFavouriteStops(List<Integer> favouriteStops) throws SQLException {
        QueryBuilder<Stop, Integer> stopsIntegerQueryBuilder = queryBuilder();
        stopsIntegerQueryBuilder.where().in("Id", favouriteStops);
        PreparedQuery<Stop> preparedQuery = stopsIntegerQueryBuilder.prepare();
        return query(preparedQuery);
    }
}
