package by.denprokazov.belbusschedule;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import by.denprokazov.belbusschedule.dbHandlers.DBInitializer;
import by.denprokazov.belbusschedule.fragments.RouteListFragment;
import by.denprokazov.belbusschedule.fragments.StopsListFragment;

public class MainActivitySlidingTab extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_sliding_view);
        initDB();
        initUI();
    }

    private void initDB() {
        // TODO: 19.02.2016 string reciever
        DBInitializer dbInitializer = new DBInitializer(this);
        dbInitializer.getWritableDatabase();
        dbInitializer.close();
        HelperFactory.setDatabaseHelper(this);
    }


    private void initUI() {
        initToolBar();
        Bundle notFavBundle = new Bundle();
        notFavBundle.putBoolean("fromFavourite", false);
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(getSupportFragmentManager(),
                FragmentPagerItems.with(this)
                        .add(R.string.route_tab_name, RouteListFragment.class, notFavBundle)
                        .add(R.string.stops_tab_name, StopsListFragment.class, notFavBundle).create());


        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);
    }

    private void initToolBar() {
        Toolbar mainToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        mainToolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(mainToolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_sliding_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_settings:
                Intent settingsIntent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(settingsIntent);
                return true;
            case R.id.action_favorite:
                Intent favouritesIntent = new Intent(getApplicationContext(), FavouritesActivity.class);
                startActivity(favouritesIntent);
                return true;
        }
        return true;
    }
}
