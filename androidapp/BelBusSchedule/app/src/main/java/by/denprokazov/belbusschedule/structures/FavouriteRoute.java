package by.denprokazov.belbusschedule.structures;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "FavouriteRoutes")
public class FavouriteRoute {
    @DatabaseField(columnName = "route_id")
    public int route_id;
}
