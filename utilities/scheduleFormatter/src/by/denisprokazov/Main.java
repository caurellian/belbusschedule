package by.denisprokazov;

import java.io.*;

public class Main {
    private static FileParser fileParser;
    private static final String DEFAULT_FILE_PATH_LINUX = "/home/denny/dev/repos/belbusschedule/sql/rawdata/autoparkdirection";

    public static void main(String[] args) throws IOException {
        fileParser = new FileParser(new FileEditor().openFile(DEFAULT_FILE_PATH_LINUX));
        fileParser.decomposeFile();
    }
}
