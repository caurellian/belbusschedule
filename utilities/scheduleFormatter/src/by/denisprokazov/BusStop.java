package by.denisprokazov;

/**
 * Created by denny on 1/29/16.
 */
public class BusStop {
    public String stopName;
    public String stopTime;
    public String direction;
    public int busId;
    public int isHoliday;
}
