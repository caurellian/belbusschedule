package by.denisprokazov;

import java.io.File;

/**
 * Created by caurellian on 1/29/16 with idea on linux.
 */
public class FileEditor {
    private static File inputFile;

    protected File openFile(String filePath) {
        inputFile = new File(filePath);
        if (inputFile.exists()) {
            System.out.println("open Succeed");
        } else {
            System.out.println("open failed");
        }
        return inputFile;
    }
    protected File createFile(String exportFilePath) {
        return new File(exportFilePath);
    }
}
