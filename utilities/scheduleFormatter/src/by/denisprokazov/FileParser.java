package by.denisprokazov;

import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by denny on 1/29/16.
 */
public class FileParser {

    private File workFile;
    private List<String> readedString;

    private Jsoninficator jsoninficator = new Jsoninficator();
    private String stopName;
    private String direction;
    private int isHoliday;

    public FileParser(File inputFile) {
        workFile = inputFile;
        makeArrayFromInput();
    }

    protected void decomposeFile() throws IOException {
        for (int counter  = 0 ; counter < readedString.size(); counter++) {
            BusStop busStop = new BusStop();
            counter = fillBasicInfo(counter, busStop);
            counter = fillIsHoliday(counter, busStop);
            fillStopTime(counter, busStop);
            fillBusId(counter, busStop);
            fillDataStructure(busStop);
            if (checkEOF(counter+1)) break;
        }
    }

    private void makeArrayFromInput() {
        readedString = new ArrayList<>();
        try {
            addStringsToArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addStringsToArray() throws IOException {
        String tmp;
        BufferedReader bufferedReader = new BufferedReader(new FileReader(workFile));
        while((tmp = bufferedReader.readLine()) != null) {
            readedString.add(tmp);
        }
    }

    private void fillDataStructure(BusStop busStop) throws IOException {
        JSONObject jsonObject = jsoninficator.makeJsonObject(busStop);
        jsoninficator.jsonToFile(jsonObject);
    }

    private void fillBusId(int counter, BusStop busStop) {
        if (readedString.get(counter).contains(" ТЕР.")) {
            busStop.busId = 2;
        } else {
            busStop.busId = 1;
        }
    }

    private boolean checkEOF(int counter) throws IOException {
        if (Objects.equals(readedString.get(counter), "--end--")) {
            jsoninficator.flushFile();
            return true;
        }
        return false;
    }

    private void fillStopTime(int counter, BusStop busStop) {
        busStop.stopTime = readedString.get(counter).replaceAll(" ТЕР\\.", "");
    }

    private int fillIsHoliday(int counter, BusStop busStop) {
        if (Objects.equals(readedString.get(counter), "ПО ВЫХОДНЫМ:")) {
            isHoliday = 1;
            counter++;
        }
        busStop.isHoliday = isHoliday;
        return counter;
    }

    private int fillBasicInfo(int counter, BusStop busStop) {
        if (Objects.equals(readedString.get(counter), "--separator--")) {
            isHoliday = 0;
            stopName = readedString.get(counter+1);
            direction = readedString.get(counter+2);
            counter = counter + 3;
        }
        busStop.stopName = stopName;
        busStop.direction = direction;
        return counter;
    }
}
