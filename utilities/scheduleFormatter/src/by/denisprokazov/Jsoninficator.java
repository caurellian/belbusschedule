package by.denisprokazov;

import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Администратор on 29.01.2016.
 */
public class Jsoninficator {
    //private static final String EXPORT_FILE_PATH_WINDOWS = "C:/export.json";
    private static final String EXPORT_FILE_PATH_LINUX = "/home/denny/dev/repos/belbusschedule/sql/rawdata/autoparkjson.json";
    private static FileEditor fileEditor = new FileEditor();
    private FileWriter fileWriter = null;

    public Jsoninficator() {
        try {
            fileWriter = new FileWriter(fileEditor.createFile(EXPORT_FILE_PATH_LINUX));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public JSONObject makeJsonObject(BusStop busStop) throws IOException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("stopName", busStop.stopName);
        jsonObject.put("stopDirection", busStop.direction);
        jsonObject.put("busId", busStop.busId);
        jsonObject.put("stopTime", busStop.stopTime);
        jsonObject.put("isHoliday", busStop.isHoliday);
        return jsonObject;
    }
    public void jsonToFile(JSONObject jsonObject) throws IOException {
        fileWriter.append(jsonObject.toJSONString());
        fileWriter.append("\n");
    }
    public void flushFile() throws IOException {
        fileWriter.flush();
    }
}
