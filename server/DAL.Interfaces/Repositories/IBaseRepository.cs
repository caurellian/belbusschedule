﻿using System;
using System.Collections.Generic;

namespace DAL.Interfaces.Repositories
{
    public interface IBaseRepository<TEntity>
    {
        IEnumerable<TEntity> Get(Func<bool> query);
    }
}