﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DomainModels.Entities
{
    public class Country: PersistentObject
    {
        public String Name
        {
            get;
            set;
        }

        [JsonIgnore]
        public ICollection<City> Cities
        {
            get;
            set;
        }
    }
}
