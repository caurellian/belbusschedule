﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DomainModels.Entities
{
    public class City: PersistentObject
    {
        public String Name
        {
            get;
            set;
        }

        public long CountryId
        {
            get;
            set;
        }

        [JsonIgnore]
        public Country Country
        {
            get;
            set;
        }

        [JsonIgnore]
        public ICollection<Route> Routes
        {
            get;
            set;
        }

        [JsonIgnore]
        public ICollection<Stop> Stops
        {
            get;
            set;
        }
    }
}
