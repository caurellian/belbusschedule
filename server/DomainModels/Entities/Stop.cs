﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DomainModels.Entities
{
    public class Stop: PersistentObject
    {
        [JsonProperty(PropertyName = "name")]
        public String Name
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "city_id")]
        public long CityId
        {
            get;
            set;
        }

        [JsonIgnore]
        public City City
        {
            get;
            set;
        }

        [JsonIgnore]
        public ICollection<StopInfo> StopInfos
        {
            get;
            set;
        }
    }
}
