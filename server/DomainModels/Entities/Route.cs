﻿using System;
using System.Collections.Generic;
using DomainModels.Enums;
using Newtonsoft.Json;

namespace DomainModels.Entities
{
    public class Route: PersistentObject
    {
        [JsonProperty(PropertyName = "name")]
        public String Name
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "type_id")]
        public TransportType TransportType
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "city_id")]
        public long CityId
        {
            get;
            set;
        }

        [JsonIgnore]
        public City City
        {
            get;
            set;
        }

        public String Description
        {
            get;
            set;
        }

        [JsonIgnore]
        public ICollection<StopInfo> StopInfos
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "begin_stop_id")]
        public long BeginStopId
        {
            get;
            set;
        }

        [JsonIgnore]
        public Stop BeginStop
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "end_stop_id")]
        public long EndStopId
        {
            get;
            set;
        }

        [JsonIgnore]
        public Stop EndStop
        {
            get;
            set;
        }
    }
}
