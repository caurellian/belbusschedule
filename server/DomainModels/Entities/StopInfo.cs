﻿using System;
using Newtonsoft.Json;

namespace DomainModels.Entities
{
    public class StopInfo: PersistentObject
    {
        [JsonProperty(PropertyName = "stop_id")]
        public long StopId
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "route_id")]
        public long RouteId
        {
            get;
            set;
        }

        [JsonIgnore]
        public Route Route
        {
            get;
            set;
        }

        [JsonIgnore]
        public Stop Stop
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "time")]
        public TimeSpan Time
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "is_holiday")]
        public bool IsHoliday
        {
            get;
            set;
        }
    }
}
