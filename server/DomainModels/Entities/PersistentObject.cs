﻿using System;
using DomainModels.Interfaces;
using Newtonsoft.Json;

namespace DomainModels.Entities
{
    public class PersistentObject: IPersistentObject
    {
        public Int64 Id
        {
            get;
            set;
        }

        public Boolean IsNew()
        {
            return false;
        }

        [JsonIgnore]
        public DateTime ModificationDate
        {
            get;
            set;
        }
    }
}
