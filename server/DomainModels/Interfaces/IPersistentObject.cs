﻿namespace DomainModels.Interfaces
{
    public interface IPersistentObject: IHasModificationDate
    {
        long Id
        {
            get;
            set;
        }

        bool IsNew();
    }
}