﻿using System;

namespace DomainModels.Interfaces
{
    public interface IHasModificationDate
    {
        DateTime ModificationDate
        {
            get;
            set;
        }
    }
}
