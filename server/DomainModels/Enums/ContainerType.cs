﻿namespace DomainModels.Enums
{
    public enum ContainerType
    {
        Country = 0,
        City = 1,
        Route = 2,
        Stop = 3,
        StopInfo = 4
    }
}
