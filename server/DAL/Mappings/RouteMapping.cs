﻿using DomainModels.Entities;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata.Builders;

namespace DAL.Mappings
{
    public class RouteMapping
    {
        public RouteMapping(EntityTypeBuilder<Route> entityBuilder)
        {
            entityBuilder.ToTable("Route");
            entityBuilder.HasKey(route => route.Id);
            entityBuilder.Property(route => route.Name);
            entityBuilder.Property(route => route.Description);
            entityBuilder.Property(route => route.TransportType);
            entityBuilder.Property(route => route.BeginStopId);
            entityBuilder.Property(route => route.EndStopId);
            entityBuilder.HasOne(route => route.City).WithMany(city => city.Routes);
            entityBuilder.HasOne(route => route.BeginStop);
            entityBuilder.HasOne(route => route.EndStop);
        }
    }
}
