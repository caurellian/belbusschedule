﻿using DomainModels.Entities;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Metadata.Builders;

namespace DAL.Mappings
{
    public class StopMapping
    {
        public StopMapping(EntityTypeBuilder<Stop> entityBuilder)
        {
            entityBuilder.ToTable("Stop");
            entityBuilder.HasKey(stop => stop.Id);
            entityBuilder.Property(stop => stop.Name);
            entityBuilder.HasOne(stop => stop.City).WithMany(city => city.Stops);
            entityBuilder.HasMany(stop => stop.StopInfos).WithOne(info => info.Stop).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
