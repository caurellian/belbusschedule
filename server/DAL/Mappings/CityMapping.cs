﻿using DomainModels.Entities;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata.Builders;

namespace DAL.Mappings
{
    public class CityMapping
    {
        public CityMapping(EntityTypeBuilder<City> entityBuilder)
        {
            entityBuilder.ToTable("City");
            entityBuilder.HasKey(city => city.Id);
            entityBuilder.Property(city => city.Name);
            entityBuilder.HasOne(city => city.Country).WithMany(county => county.Cities);
        }
    }
}
