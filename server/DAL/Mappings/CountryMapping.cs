﻿using DomainModels.Entities;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata.Builders;

namespace DAL.Mappings
{
    public class CountryMapping
    {
        public CountryMapping(EntityTypeBuilder<Country> entityBuilder)
        {
            entityBuilder.ToTable("Country");
            entityBuilder.HasKey(city => city.Id);
            entityBuilder.Property(city => city.Name);
        }
    }
}
