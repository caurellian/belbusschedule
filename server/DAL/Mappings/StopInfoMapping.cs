﻿using DomainModels.Entities;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Metadata.Builders;

namespace DAL.Mappings
{
    public class StopInfoMapping
    {
        public StopInfoMapping(EntityTypeBuilder<StopInfo> entityBuilder)
        {
            entityBuilder.ToTable("StopInfo");
            entityBuilder.HasKey(stopInfo => stopInfo.Id);
            entityBuilder.Property(stopInfo => stopInfo.Time);
            entityBuilder.HasOne(stopInfo => stopInfo.Route).WithMany(route => route.StopInfos).OnDelete(DeleteBehavior.Restrict);
            entityBuilder.HasOne(stopInfo => stopInfo.Stop).WithMany(stop => stop.StopInfos).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
