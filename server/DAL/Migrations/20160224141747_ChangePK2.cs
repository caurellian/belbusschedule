using Microsoft.Data.Entity.Migrations;
using Microsoft.Data.Entity.Metadata;

namespace DAL.Migrations
{
    public partial class ChangePK2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_City_Country_CountryId", table: "City");
            migrationBuilder.DropForeignKey(name: "FK_Route_City_CityId", table: "Route");
            migrationBuilder.DropForeignKey(name: "FK_Stop_City_CityId", table: "Stop");
            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "StopInfo",
                nullable: false)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);
            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "Stop",
                nullable: false)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);
            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "Country",
                nullable: false)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);
            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "City",
                nullable: false)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);
            migrationBuilder.AddForeignKey(
                name: "FK_City_Country_CountryId",
                table: "City",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Route_City_CityId",
                table: "Route",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Stop_City_CityId",
                table: "Stop",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_City_Country_CountryId", table: "City");
            migrationBuilder.DropForeignKey(name: "FK_Route_City_CityId", table: "Route");
            migrationBuilder.DropForeignKey(name: "FK_Stop_City_CityId", table: "Stop");
            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "StopInfo",
                nullable: false);
            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "Stop",
                nullable: false);
            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "Country",
                nullable: false);
            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "City",
                nullable: false);
            migrationBuilder.AddForeignKey(
                name: "FK_City_Country_CountryId",
                table: "City",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Route_City_CityId",
                table: "Route",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Stop_City_CityId",
                table: "Stop",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
