using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DAL;

namespace DAL.Migrations
{
    [DbContext(typeof(ApplicationContext))]
    [Migration("20160224160142_Changes")]
    partial class Changes
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DomainModels.Entities.City", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CountryId");

                    b.Property<DateTime>("ModificationDate");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "City");
                });

            modelBuilder.Entity("DomainModels.Entities.Country", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("ModificationDate");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "Country");
                });

            modelBuilder.Entity("DomainModels.Entities.Route", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CityId");

                    b.Property<string>("Description");

                    b.Property<DateTime>("ModificationDate");

                    b.Property<string>("Name");

                    b.Property<int>("TransportType");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "Route");
                });

            modelBuilder.Entity("DomainModels.Entities.Stop", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CityId");

                    b.Property<DateTime>("ModificationDate");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "Stop");
                });

            modelBuilder.Entity("DomainModels.Entities.StopInfo", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsHoliday");

                    b.Property<DateTime>("ModificationDate");

                    b.Property<long>("RouteId");

                    b.Property<long>("StopId");

                    b.Property<TimeSpan>("Time");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "StopInfo");
                });

            modelBuilder.Entity("DomainModels.Entities.City", b =>
                {
                    b.HasOne("DomainModels.Entities.Country")
                        .WithMany()
                        .HasForeignKey("CountryId");
                });

            modelBuilder.Entity("DomainModels.Entities.Route", b =>
                {
                    b.HasOne("DomainModels.Entities.City")
                        .WithMany()
                        .HasForeignKey("CityId");
                });

            modelBuilder.Entity("DomainModels.Entities.Stop", b =>
                {
                    b.HasOne("DomainModels.Entities.City")
                        .WithMany()
                        .HasForeignKey("CityId");
                });

            modelBuilder.Entity("DomainModels.Entities.StopInfo", b =>
                {
                    b.HasOne("DomainModels.Entities.Route")
                        .WithMany()
                        .HasForeignKey("RouteId");

                    b.HasOne("DomainModels.Entities.Stop")
                        .WithMany()
                        .HasForeignKey("StopId");
                });
        }
    }
}
