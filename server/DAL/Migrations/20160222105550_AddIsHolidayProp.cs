using Microsoft.Data.Entity.Migrations;

namespace DAL.Migrations
{
    public partial class AddIsHolidayProp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_City_Country_CountryId", table: "City");
            migrationBuilder.DropForeignKey(name: "FK_Route_City_CityId", table: "Route");
            migrationBuilder.DropForeignKey(name: "FK_Stop_City_CityId", table: "Stop");
            migrationBuilder.AddColumn<bool>(
                name: "IsHoliday",
                table: "StopInfo",
                nullable: false,
                defaultValue: false);
            migrationBuilder.AddForeignKey(
                name: "FK_City_Country_CountryId",
                table: "City",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Route_City_CityId",
                table: "Route",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Stop_City_CityId",
                table: "Stop",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_City_Country_CountryId", table: "City");
            migrationBuilder.DropForeignKey(name: "FK_Route_City_CityId", table: "Route");
            migrationBuilder.DropForeignKey(name: "FK_Stop_City_CityId", table: "Stop");
            migrationBuilder.DropColumn(name: "IsHoliday", table: "StopInfo");
            migrationBuilder.AddForeignKey(
                name: "FK_City_Country_CountryId",
                table: "City",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Route_City_CityId",
                table: "Route",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Stop_City_CityId",
                table: "Stop",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
