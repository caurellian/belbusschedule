using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;
using Microsoft.Data.Entity.Metadata;

namespace DAL.Migrations
{
    public partial class deleteDirections_ChangeMappings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
//            migrationBuilder.DropForeignKey(name: "FK_City_Country_CountryId", table: "City");
//            migrationBuilder.DropForeignKey(name: "FK_Route_City_CityId", table: "Route");
            migrationBuilder.DropForeignKey(name: "FK_Route_Direction_DirectionId", table: "Route");
//            migrationBuilder.DropForeignKey(name: "FK_Stop_City_CityId", table: "Stop");
            migrationBuilder.DropColumn(name: "DirectionId", table: "Route");
            migrationBuilder.DropTable("Direction");
            migrationBuilder.AddColumn<long>(
                name: "BeginStopId",
                table: "Route",
                nullable: false,
                defaultValue: 0L);
            migrationBuilder.AddColumn<long>(
                name: "EndStopId",
                table: "Route",
                nullable: false,
                defaultValue: 0L);
//            migrationBuilder.AddForeignKey(
//                name: "FK_City_Country_CountryId",
//                table: "City",
//                column: "CountryId",
//                principalTable: "Country",
//                principalColumn: "Id",
//                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Route_Stop_BeginStopId",
                table: "Route",
                column: "BeginStopId",
                principalTable: "Stop",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
//            migrationBuilder.AddForeignKey(
//                name: "FK_Route_City_CityId",
//                table: "Route",
//                column: "CityId",
//                principalTable: "City",
//                principalColumn: "Id",
//                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Route_Stop_EndStopId",
                table: "Route",
                column: "EndStopId",
                principalTable: "Stop",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
//            migrationBuilder.AddForeignKey(
//                name: "FK_Stop_City_CityId",
//                table: "Stop",
//                column: "CityId",
//                principalTable: "City",
//                principalColumn: "Id",
//                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
//            migrationBuilder.DropForeignKey(name: "FK_City_Country_CountryId", table: "City");
            migrationBuilder.DropForeignKey(name: "FK_Route_Stop_BeginStopId", table: "Route");
//            migrationBuilder.DropForeignKey(name: "FK_Route_City_CityId", table: "Route");
            migrationBuilder.DropForeignKey(name: "FK_Route_Stop_EndStopId", table: "Route");
//            migrationBuilder.DropForeignKey(name: "FK_Stop_City_CityId", table: "Stop");
            migrationBuilder.DropColumn(name: "BeginStopId", table: "Route");
            migrationBuilder.DropColumn(name: "EndStopId", table: "Route");
            migrationBuilder.CreateTable(
                name: "Direction",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ModificationDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Direction", x => x.Id);
                });
            migrationBuilder.AddColumn<long>(
                name: "DirectionId",
                table: "Route",
                nullable: false,
                defaultValue: 0L);
//            migrationBuilder.AddForeignKey(
//                name: "FK_City_Country_CountryId",
//                table: "City",
//                column: "CountryId",
//                principalTable: "Country",
//                principalColumn: "Id",
//                onDelete: ReferentialAction.Restrict);
//            migrationBuilder.AddForeignKey(
//                name: "FK_Route_City_CityId",
//                table: "Route",
//                column: "CityId",
//                principalTable: "City",
//                principalColumn: "Id",
//                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Route_Direction_DirectionId",
                table: "Route",
                column: "DirectionId",
                principalTable: "Direction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
//            migrationBuilder.AddForeignKey(
//                name: "FK_Stop_City_CityId",
//                table: "Stop",
//                column: "CityId",
//                principalTable: "City",
//                principalColumn: "Id",
//                onDelete: ReferentialAction.Restrict);
        }
    }
}
