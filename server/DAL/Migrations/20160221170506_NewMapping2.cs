using System;
using Microsoft.Data.Entity.Migrations;
using Microsoft.Data.Entity.Metadata;

namespace DAL.Migrations
{
    public partial class NewMapping2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_City_County_CountryId", table: "City");
            migrationBuilder.DropForeignKey(name: "FK_Route_City_CityId", table: "Route");
            migrationBuilder.DropForeignKey(name: "FK_Stop_City_CityId", table: "Stop");
            migrationBuilder.DropForeignKey(name: "FK_StopInfo_Route_RouteId", table: "StopInfo");
            migrationBuilder.DropForeignKey(name: "FK_StopInfo_Stop_StopId", table: "StopInfo");
            migrationBuilder.DropTable("County");
            migrationBuilder.CreateTable(
                name: "Country",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ModificationDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.Id);
                });
            migrationBuilder.AddForeignKey(
                name: "FK_City_Country_CountryId",
                table: "City",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Route_City_CityId",
                table: "Route",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Stop_City_CityId",
                table: "Stop",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_StopInfo_Route_RouteId",
                table: "StopInfo",
                column: "RouteId",
                principalTable: "Route",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
            migrationBuilder.AddForeignKey(
                name: "FK_StopInfo_Stop_StopId",
                table: "StopInfo",
                column: "StopId",
                principalTable: "Stop",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_City_Country_CountryId", table: "City");
            migrationBuilder.DropForeignKey(name: "FK_Route_City_CityId", table: "Route");
            migrationBuilder.DropForeignKey(name: "FK_Stop_City_CityId", table: "Stop");
            migrationBuilder.DropForeignKey(name: "FK_StopInfo_Route_RouteId", table: "StopInfo");
            migrationBuilder.DropForeignKey(name: "FK_StopInfo_Stop_StopId", table: "StopInfo");
            migrationBuilder.DropTable("Country");
            migrationBuilder.CreateTable(
                name: "County",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ModificationDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_County", x => x.Id);
                });
            migrationBuilder.AddForeignKey(
                name: "FK_City_County_CountryId",
                table: "City",
                column: "CountryId",
                principalTable: "County",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Route_City_CityId",
                table: "Route",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Stop_City_CityId",
                table: "Stop",
                column: "CityId",
                principalTable: "City",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_StopInfo_Route_RouteId",
                table: "StopInfo",
                column: "RouteId",
                principalTable: "Route",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_StopInfo_Stop_StopId",
                table: "StopInfo",
                column: "StopId",
                principalTable: "Stop",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
