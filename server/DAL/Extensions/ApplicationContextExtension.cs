﻿using System;
using DomainModels.Entities;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.ChangeTracking;
using Microsoft.Data.Entity.Infrastructure;

namespace DAL.Extensions
{
    public static class ApplicationContextExtension
    {
        public static TEntity SaveOrUpdate<TEntity>(this ApplicationContext applicationContext, TEntity entity)
            where TEntity: PersistentObject, new()
        {
            if (entity.IsNew())
            {
                EntityEntry<TEntity> addedEntity = applicationContext.Add(entity);
                return addedEntity.Entity;
            }
            else
            {
                EntityEntry<TEntity> updatedEntity = applicationContext.Update(entity);
                return updatedEntity.Entity;
            }
        }
        public static void SetIdentityInsert(this DatabaseFacade database, String tableName, bool state = true)
        {
            String status = state ? "ON" : "OFF";
            database.ExecuteSqlCommand($"SET IDENTITY_INSERT {tableName} {status}");
        }
    }
}
