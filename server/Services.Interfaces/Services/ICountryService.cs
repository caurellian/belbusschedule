﻿using System;
using System.Collections.Generic;
using DomainModels.Entities;

namespace Services.Interfaces.Services
{
    public interface ICountryService
    {
        IEnumerable<Country> GetCountries(DateTime? date);
    }
}
