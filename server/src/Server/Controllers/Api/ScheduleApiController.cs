﻿using System;
using System.Linq;
using DAL;
using DomainModels.Entities;
using DomainModels.Interfaces;
using Microsoft.AspNet.Mvc;
using Newtonsoft.Json;

namespace Server.Controllers.Api
{
    public class ScheduleApiController: Controller
    {
        private readonly ApplicationContext context;

        public ScheduleApiController(ApplicationContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public String GetCountries(DateTime? modificationDate)
        {
            IQueryable<Country> result = context.Countries.Where(country => FilterEntities(country, modificationDate));
            return JsonConvert.SerializeObject(result);
        }

        [HttpGet]
        public String GetCities(DateTime? modificationDate)
        {
            IQueryable<City> result = context.Cities.Where(city => FilterEntities(city, modificationDate));
            return JsonConvert.SerializeObject(result);
        }

        [HttpGet]
        public String GetRoutes(DateTime? modificationDate)
        {
            IQueryable<Route> result = context.Routes.Where(route => FilterEntities(route, modificationDate));
            return JsonConvert.SerializeObject(result);
        }

        [HttpGet]
        public String GetStops(DateTime? modificationDate)
        {
            IQueryable<Stop> result = context.Stops.Where(stop => FilterEntities(stop, modificationDate));
            return JsonConvert.SerializeObject(result);
        }

        [HttpGet]
        public String GetStopInfos(DateTime? modificationDate)
        {
            IQueryable<StopInfo> result = context.StopInfos.Where(stopInfo => FilterEntities(stopInfo, modificationDate));
            return JsonConvert.SerializeObject(result);
        }

        private bool FilterEntities(IHasModificationDate entity, DateTime? modificationDate)
        {
            return !modificationDate.HasValue || entity.ModificationDate > modificationDate;
        }
    }
}
