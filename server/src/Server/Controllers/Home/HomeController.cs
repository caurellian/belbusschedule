﻿using Microsoft.AspNet.Mvc;

namespace Server.Controllers.Home
{
    public class HomeController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View("Index");
        }
    }
}
