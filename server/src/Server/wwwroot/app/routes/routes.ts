﻿import {Component} from 'angular2/core';
import {Router, RouterLink} from 'angular2/router';
import {CORE_DIRECTIVES, FORM_DIRECTIVES} from 'angular2/common';
import {Route} from '../core/domain/route';
import {DataService} from '../core/services/dataService';
import {bootstrap}   from 'angular2/platform/browser';
import {List}   from 'linqts';

@Component({
    selector: 'routes',
    providers: [DataService],
    templateUrl: './app/routes/routes.html',
    bindings: [DataService],
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, RouterLink]
})
export class RoutesComponent {
    private _apiUri: string = '/ScheduleApi/GetRoutes';
    private _routes: Array<Route>;

    constructor(public service: DataService) {
        service.set(this._apiUri);
        this._getRoutes();
    }

    private _getRoutes() {
        this.service.get()
            .subscribe(res => {

                this._routes = res.json();
            },
            error => console.error('Error: ' + error));
    }

    public get distinctRoutes() {
        if (this._routes)
        {
            var res = new List<Route>(this._routes).Select(r => r.name).Distinct(r=>r);
            return res.ToArray();
        }
        return null;
    }

    public onSelect(name: string): void {
        debugger;
    }
}