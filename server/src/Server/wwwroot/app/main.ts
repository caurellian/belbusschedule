﻿///<reference path="../../node_modules/angular2/typings/browser.d.ts"/>
/// <reference path="../../node_modules/angular2/core.d.ts" />

import { Component } from 'angular2/core';
import {bootstrap}   from 'angular2/platform/browser';
import {RoutesComponent}   from './routes/routes';

import {HTTP_PROVIDERS} from 'angular2/http';

@Component({
    selector: 'schedule-app',
    templateUrl: './app/main.html',
    directives: [RoutesComponent]
})

export class Home {

    constructor() {
    }
}

bootstrap(Home, [HTTP_PROVIDERS]);