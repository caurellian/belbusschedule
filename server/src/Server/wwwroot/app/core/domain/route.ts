﻿import {TransportType} from './TransportType';

export class Route {
    constructor(public name: string,
        public type_id: TransportType,
        public city_id: number,
        public Description: string,
        public begin_stop_id: number,
        public end_stop_id: number
    ) {
    }
}