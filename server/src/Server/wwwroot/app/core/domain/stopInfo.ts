﻿export class StopInfo {
    constructor(public StopId: number,
        public RouteId: number,
        public Time: Date,
        public IsHoliday: Boolean) {
        
    }
}