﻿import { Http, Response, Request } from 'angular2/http';
import { Injectable } from 'angular2/core';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

    public _baseUri: string;

    constructor(public http: Http) {
    }

    set(baseUri: string): void {
        this._baseUri = baseUri;
    }

    get() {
        return this.http.get(this._baseUri)
            .map(response => (<Response>response));
    }
}