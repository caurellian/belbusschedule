﻿import {Component} from 'angular2/core';
import {Router, RouterLink} from 'angular2/router';
import {CORE_DIRECTIVES, FORM_DIRECTIVES} from 'angular2/common';
import {Stop} from '../core/domain/stop';
import {StopInfo} from '../core/domain/stopInfo';
import {DataService} from '../core/services/dataService';
import {bootstrap}   from 'angular2/platform/browser';

@Component({
    selector: 'stops',
    providers: [DataService],
    templateUrl: './app/stops/stops.html',
    bindings: [DataService],
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, RouterLink]
})
export class SopsComponent {
    private _stopsApiUri: string = '/ScheduleApi/GetStops';
    private _stopInfosApiUri: string = '/ScheduleApi/GetStops';
    private _stops: Array<Stop>;
    private _stopInfos: Array<StopInfo>;

    constructor(public service: DataService) {
        service.set(this._stopsApiUri);
        this._getStops();
        service.set(this._stopInfosApiUri);
        this._getStopInfos();
    }

    private _getStops() {
        this.service.get()
            .subscribe(res => {

                var data: any = res.json();
                    debugger;
            },
            error => console.error('Error: ' + error));
    }

    private _getStopInfos() {
        this.service.get()
            .subscribe(res => {

                var data: any = res.json();
                    debugger;
            },
            error => console.error('Error: ' + error));
    }
}